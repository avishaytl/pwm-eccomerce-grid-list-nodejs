"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addNewItem = exports.getListData = void 0;
const connectDB = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const mysql = require('mysql');
        let connection = yield mysql.createConnection({
            host: `localhost`,
            user: 'root',
            password: '',
            database: 'presolu3_pwm'
        });
        yield connection.connect(function (err) {
            if (err)
                console.error(err.stack);
            console.log('connected as id ' + connection.threadId);
        });
        return connection;
    }
    catch (error) {
        throw error;
    }
});
const getListData = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res);
        let errMsg = '';
        let errCode = 'c1$23s5-';
        let isCreated = true;
        yield connection.query(`select * from List`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'select from';
                isCreated = false;
                console.debug('getListData select from error, ' + error.sqlMessage);
            }
            else
                isCreated = result;
            console.debug('getListData select from res', result);
        });
        yield connection.end(function (err) {
            if (err) {
                isCreated = false;
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection getListData');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated });
            else
                res.status(400).json({ code: 400, msg: 'getListData error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.getListData = getListData;
const addNewItem = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let connection = yield connectDB(req, res);
        let errMsg = '';
        let errCode = 'c1$23s4-';
        let isCreated = true;
        yield connection.query(`create table List (
        id VARCHAR(255),
        img VARCHAR(255),
        des VARCHAR(255),
        rate VARCHAR(255),
        price VARCHAR(255),
        info VARCHAR(255),
        PRIMARY KEY (id))`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'create table';
                isCreated = false;
                console.debug('addNewItem create table error, ' + error.sqlMessage);
            }
            ;
            console.debug('addNewItem create table res', result);
        });
        if (!req.body.img || !req.body.price || !req.body.des || !req.body.rate || !req.body.info)
            return res.status(400).json({ code: 400, msg: 'addNewItem error, ' + 'item must includes img, price, des, rate, info!', errCode: errCode });
        yield connection.query(`insert into List (id, img, des, price, rate, info) values (
        '${makeid(5)}','${req.body.img}','${req.body.des}','${req.body.price}','${req.body.rate}','${req.body.info}')`, function (error, result, fields) {
            if (error) {
                errMsg = error.sqlMessage;
                errCode += 'insert into';
                isCreated = false;
                console.debug('addNewItem insert into error, ' + error.sqlMessage);
            }
            else {
                isCreated = true;
                errMsg = '';
            }
            console.debug('addNewItem insert into res', result);
        });
        yield connection.end(function (err) {
            if (err) {
                isCreated = false;
                res.status(400).json({ code: 400, msg: 'end connection error' });
                throw err;
            }
            ;
            console.debug('end connection addNewItem');
            if (errMsg === '')
                res.status(200).json({ code: 200, response: isCreated });
            else
                res.status(400).json({ code: 400, msg: 'addNewItem error, ' + errMsg, errCode: errCode });
        });
    }
    catch (error) {
        throw error;
    }
});
exports.addNewItem = addNewItem;
function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}
