import express, { Express } from "express"
import cors from "cors"
import blogRoutes from "./routes"

const app: Express = express()

const PORT: number = 4000

app.use(cors())
app.use(blogRoutes) 

app.listen(PORT, () =>
  console.log(`Server running on http://localhost:${PORT}`)
)
 