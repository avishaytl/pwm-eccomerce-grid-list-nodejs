import { Router } from "express"
import * as controllers from "../controllers/index"
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()

const router: Router = Router()
 
router.get("/getListData", controllers.main.getListData)
router.post("/addNewItem", jsonParser, controllers.main.addNewItem)  

export default router